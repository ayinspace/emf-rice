package space.ayin.emf_rice;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EPackage;

import java.util.*;

public class RiceRegistryInst implements RiceRegistry {

    private Map<EClass, List<EClass>> inheritanceMap = new HashMap<>();

    public RiceRegistryInst( EClass eClass) {
        EPackage ePackage = eClass.getEPackage();
        List<EClassifier> eClassifiers = ePackage.getEClassifiers();
        for(EClassifier eCl : eClassifiers){
            if(eCl instanceof EClass)
            inheritanceMap.put((EClass)eCl, new ArrayList<>());
        }
        for(EClass key : inheritanceMap.keySet()){
            for(EClass superClass : key.getEAllSuperTypes()){
                inheritanceMap.get(superClass).add(key);
            }
        }
        for(List<EClass> eList : inheritanceMap.values()){
            eList.sort(Comparator.comparing(ENamedElement::getName));
        }
        //TODO: only one package is scanned for now
    }

    @Override
    public EClass resolveToClass( EClass fieldType, int number) throws IllegalArgumentException {
        EClass result;
        if(inheritanceMap.containsKey(fieldType)) {
            List<EClass> list = inheritanceMap.get(fieldType);
            if(list == null)
                throw new RuntimeException("wrong inheritance map state, null instead of empty list");
            if(number == 0)
                return fieldType;
            if(number > list.size())
                throw new IllegalArgumentException("EClass subclass wrong number - too large");
            result = inheritanceMap.get(fieldType).get(number - 1);
        }
        else
            throw new IllegalArgumentException("EClass not registered");
        return result;
    }

    @Override
    public int resolveToNumber(EClass fieldType, EClass objectType) {
        if(Objects.equals(fieldType, objectType))
            return 0;
        int result;
        if(inheritanceMap.containsKey(fieldType)) {
            List<EClass> list = inheritanceMap.get(fieldType);
            if(list == null)
                throw new RuntimeException("wrong inheritance map state, null instead of empty list");
            result = list.indexOf(objectType)+1;
        }
        else
            throw new IllegalArgumentException("EClass not registered");
        return result;
    }

    @Override
    public int capacity( EClass fieldType) throws IllegalArgumentException{
        int result = -1;
        if(inheritanceMap.containsKey(fieldType)){
            List<EClass> list = inheritanceMap.get(fieldType);
            if(list == null)
                throw new RuntimeException("wrong inheritance map state, null instead of empty list");
            result = list.size() + 1;
        }
        else throw new IllegalArgumentException("EClass not registered");
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("RiceRegistryInst");
        sb.append("{hash:"+sb.hashCode()+"}");
        for(EClass keyEClass : inheritanceMap.keySet()){
            sb.append("\n");
            sb.append(keyEClass.getName());
            sb.append("(parent)\nchildren: ");
            for(EClass childEClass : inheritanceMap.get(keyEClass)){
                sb.append(childEClass.getName()).append(" ");
            }
        }
        return new String(sb);
    }
}
