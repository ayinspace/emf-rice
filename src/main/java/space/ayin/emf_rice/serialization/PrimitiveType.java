package space.ayin.emf_rice.serialization;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EcorePackage;

public enum PrimitiveType {
    Boolean(1, EcorePackage.eINSTANCE.getEBoolean(), java.lang.Boolean.TYPE),
    Byte(8, EcorePackage.eINSTANCE.getEByte(), java.lang.Byte.TYPE),
    Char(16, EcorePackage.eINSTANCE.getEChar(), java.lang.Character.TYPE),
    Short(16, EcorePackage.eINSTANCE.getEShort(), java.lang.Short.TYPE),
    Integer(32, EcorePackage.eINSTANCE.getEInt(), java.lang.Integer.TYPE),
    Float(32, EcorePackage.eINSTANCE.getEFloat(), java.lang.Float.TYPE),
    Long(64, EcorePackage.eINSTANCE.getELong(), java.lang.Long.TYPE),
    Double(64, EcorePackage.eINSTANCE.getEDouble(), java.lang.Double.TYPE),
    String(0, EcorePackage.eINSTANCE.getEString(), java.lang.String.class);

    private int length;
    private EDataType eDataType;
    private Class javaClass;

    PrimitiveType(int length, EDataType eDataType, Class javaClass) {
        this.length = length;
        this.eDataType = eDataType;
        this.javaClass = javaClass;
    }

    public static PrimitiveType typeFor(EDataType eDataType){
        for(PrimitiveType typeIter : PrimitiveType.values()){
            if(typeIter.geteDataType().equals(eDataType))
                return typeIter;
        }
        throw new RuntimeException("unreachable statement or no primitiveType for eDataType");
    }

    public static PrimitiveType typeFor(Class classType){
        for(PrimitiveType typeIter : PrimitiveType.values()){
            if(typeIter.getJavaClass().equals(classType))
                return typeIter;
        }
        throw new RuntimeException("unreachable statement or no eDataType for primitive Class");
    }

    public int getLength() {
        return length;
    }

    public EDataType geteDataType() {
        return eDataType;
    }

    public Class getJavaClass() {
        return javaClass;
    }
}
