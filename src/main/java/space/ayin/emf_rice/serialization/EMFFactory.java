package space.ayin.emf_rice.serialization;

import javafx.util.Pair;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.emf_rice.RiceRegistry;
import space.ayin.emf_rice.serialization.entities.PoolPrimitive;
import space.ayin.emf_rice.serialization.entities.ProxyObject;
import space.ayin.emf_rice.serialization.entities.ReferenceObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class EMFFactory {
    public static Logger log = LoggerFactory.getLogger(EMFFactory.class);
    private RiceRegistry registry;
    private EFactory eFactory;

    public static boolean isPrimitive(EClass eClass) {
        return (eClass.getName().equals("EAttribute"));//todo: test
    }

    public static List<Pair<EClass, List<EObject>>> containedLists(EObject eObject) {
        EClass objectClass = classOf(eObject);
        List<Pair<EClass, List<EObject>>> result = new ArrayList<>();
        EList<EReference> references = objectClass.getEAllReferences();
        Pair<EClass, List<EObject>> pair;

        for (EReference ref : references) {

            if (ref.getUpperBound() == 1) {
                continue;
            }

            EList eList = (EList) eObject.eGet(ref);

            List<EObject> list = new LinkedList<>();
            for (Object obj : eList) {
                list.add((EObject) obj);
            }

            pair = new Pair<>(ref.getEReferenceType(), list);
            result.add(pair);
            //todo: check order consistency , check nulls
        }
        return result;
    }

    public static List<Pair<EClass, EObject>> containedReferences(EObject eObject) {
        EClass objectClass = classOf(eObject);
        List<Pair<EClass, EObject>> result = new ArrayList<>();
        EList<EReference> references = objectClass.getEAllReferences();
        Pair<EClass, EObject> pair;
        for (EReference ref : references) {

            if (ref.getUpperBound() != 1) {
                continue;
            }

            Object obj = eObject.eGet(ref);

            pair = new Pair<>(ref.getEReferenceType(), (EObject) obj);
            result.add(pair);
            //todo: check order consistency , check nulls
        }
        return result;
    }

    public static List<Pair<EDataType, Object>> containedAttributes(EObject eObject) {
        EClass objectClass = classOf(eObject);
        List<Pair<EDataType, Object>> result = new ArrayList<>();
        EList<EAttribute> attributes = objectClass.getEAllAttributes();
        Pair<EDataType, Object> pair;
        for (EAttribute attribute : attributes) {
            pair = new Pair<>(attribute.getEAttributeType(), eObject.eGet(attribute));
            result.add(pair);
            //todo: check order consistency
        }
        return result;
    }

    public static List<EDataType> attributesTypesOfEObjectsEClass(EClass objectClass) {
        List<EDataType> result = new ArrayList<>();
        for (EAttribute attribute : objectClass.getEAllAttributes()) {
            result.add(attribute.getEAttributeType());
        }
        return result;
    }

    public static List<EClass> referencesTypesOfEObjectsEClass(EClass objectClass) {
        List<EClass> result = new ArrayList<>();
        for (EReference ref : objectClass.getEAllReferences()) {
            if (ref.getUpperBound() == 1)
                result.add(ref.getEReferenceType());
        }
        return result;
    }

    public static List<EClass> listsTypesOfEObjectsEClass(EClass objectClass){
        List<EClass> result = new ArrayList<>();
        for (EReference ref : objectClass.getEAllReferences()) {
            if (ref.getUpperBound() != 1)
                result.add(ref.getEReferenceType());
        }
        return result;
    }

    public static EClass classOf(EObject eObject) {
        return eObject.eClass();
    }

    public static boolean equals(EObject eObject1, EObject eObject2) {
        return EcoreUtil.equals(eObject1, eObject2);
    }

    public EObject init(EClass fieldType, DeserializationContext pool){
        log.debug("init...");
        EPackage ePackage = fieldType.getEPackage();
        eFactory = ePackage.getEFactoryInstance();

        List<ProxyObject> proxyObjects = pool.getObjects();
        List<ReferenceObject> refs = pool.getRefs();
        ReferenceObject firstRef = refs.get(0);

        return createObject(refs.get(0).getProxyObject(), null);
    }

    private EObject createObject(ProxyObject proxyObject, EObject eObject){
        log.debug("createObject {}, {}", proxyObject, eObject);
        EClass factType = proxyObject.getObjectType();
        if(eObject == null){
            eObject = eFactory.create(factType);
            createdObjects.add(eObject);
        }

        int index = 0;
        for(EAttribute attribute : factType.getEAllAttributes()){
            PoolPrimitive primitive = proxyObject.getPrimitiveRefs().get(index).getPrimitive();
            Object object = primitive.getObject();
            eObject.eSet(attribute, object);
            index++;
        }

        index = 0;
        for(EReference eReference : factType.getEAllReferences()){
            if (eReference.getUpperBound() != 1) {
                continue;
            }
            ReferenceObject ref = proxyObject.getObjectRefs().get(index);
            if(ref.isNull())
                eObject.eSet(eReference, null);
            else{
                ProxyObject memberObject = ref.getProxyObject();
                if(createdObjects.size() > memberObject.getPoolAddress())
                    eObject.eSet(eReference, createdObjects.get(memberObject.getPoolAddress()));
                else{
                    EObject newEObject = eFactory.create(memberObject.getObjectType());
                    eObject.eSet(eReference, newEObject);
                    createdObjects.add(newEObject);
                    createObject(memberObject, newEObject);
                }
            }
        }

        index = 0;
        for(EReference eReference : factType.getEAllContainments()){
            List list = new LinkedList();//(EList) eFactory.create(eReference.getEReferenceType());
//            for(ReferenceObject referenceObject : proxyObject.getObjectLists().get(index).getList()){
////                todo:add entity to EList
//                list.add(referenceObject.getProxyObject().getEmfObject());
//            }
            eObject.eSet(eReference, list);
            index++;
        }



       return eObject;
    }

    private ArrayList<EObject> createdObjects = new ArrayList();
}
