package space.ayin.emf_rice.serialization;

import com.ubs_soft.commons.collections.BitIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.emf_rice.RiceRegistry;
import space.ayin.emf_rice.RiceRegistryInst;
import space.ayin.emf_rice.serialization.entities.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DeserializationContext {
    public static Logger log = LoggerFactory.getLogger(DeserializationContext.class);

    public DeserializationContext(BitIterator refsBlockIterator, BitIterator objectsBlockIterator, EClass eClass, int numberOfObjects) {
        log.debug("creating... {}\n {}\n {}, {}", refsBlockIterator, objectsBlockIterator, eClass, numberOfObjects);
        riceRegistry = new RiceRegistryInst(eClass);
        log.debug("created riceRegistry={}", riceRegistry);
        this.refsBlockIterator = refsBlockIterator;
        this.objectsBlockIterator = objectsBlockIterator;
        this.numberOfObjects = numberOfObjects;
        readFirstRef(eClass);
    }

    private void readFirstRef(EClass eClass) throws RuntimeException {
        log.debug("readFirstRef {}", eClass);
        ReferenceObject reference = readNextRefObject(eClass);
        EClass objectActualType;// = riceRegistry.resolveToClass(eClass, reference.getRelativeTypeId());
        int counter = 0;
        while (objectsProxyList.size() < numberOfObjects) {
//        for(int counter=0; counter<numberOfObjects-1; ){

//            if(objectsProxyList.size() == numberOfObjects)
//                break;
//            reference = refsOneByObject.get(counter);
             reference = refsOneByObject.get(counter);
            objectActualType = riceRegistry.resolveToClass(reference.getRefType(), reference.getRelativeTypeId());
            readNextProxyObject(objectActualType);
            counter++;
        }

        for (ReferenceObject ref : tempRefs) {
            if (ref.isTemp())
                ref.makeNotTemp(objectsProxyList.get(ref.getTempAddr()));
        }
    }

    private ReferenceObject readNextRefObject(EClass eClass) {
        log.debug("readNextRefObject {}", eClass);
        int capacity = riceRegistry.capacity(eClass);
        if (capacity == 0)
            throw new RuntimeException("unreachable statement");
        int relativeTypeId;
        if (capacity == 1)
            relativeTypeId = 0;
        else
            relativeTypeId = refsBlockIterator.nextKnowingMax(capacity);
        ReferenceObject result = ReferenceObject.getTempReferenceObject(eClass, refsOneByObject.size());
        result.setRelativeTypeId(relativeTypeId);
        refsOneByObject.add(result);
        tempRefs.add(result);
        return result;
    }

    //read primiteves and refs from main section of current BitBuffer. refs created in temporary variant
    private void readNextProxyObject(EClass actualEClass) {
        log.debug("readNextProxyObject {}", actualEClass);
        ProxyObject proxyObject = new ProxyObject(objectsProxyList.size());
        proxyObject.setObjectType(actualEClass);


        for (EDataType dataType : EMFFactory.attributesTypesOfEObjectsEClass(actualEClass)) {
            PrimitiveType type = PrimitiveType.typeFor(dataType);
            PoolPrimitive primitive = PoolPrimitiveFactory.createPrimitive(objectsBlockIterator, type);
            int relativeTypeId = 0;
            proxyObject.addPrimitiveRef(new ReferencePrimitive(relativeTypeId, primitive));
        }

        for (EClass memberObjectType : EMFFactory.referencesTypesOfEObjectsEClass(actualEClass)) {
            boolean isNull = !objectsBlockIterator.nextBoolean();
            if (isNull)
                proxyObject.addObjectRef(ReferenceObject.getNullReferenceObject(memberObjectType));
            else {
                int objectAddress = objectsBlockIterator.nextKnowingMax(numberOfObjects);
                ReferenceObject ref;

                //the first time we read ref for this object is the only time we can recognize its type
                if (objectAddress == refsOneByObject.size())
                    ref = readNextRefObject(memberObjectType);
                else {
                    ref = ReferenceObject.getTempReferenceObject(memberObjectType, objectAddress);
                    tempRefs.add(ref);
                }
                proxyObject.addObjectRef(ref);
            }
        }

        for (EClass eClass : EMFFactory.listsTypesOfEObjectsEClass(actualEClass)) {
            //todo: make lists
        }

        objectsProxyList.add(proxyObject);

        //return proxyObject;
    }

    public List<ProxyObject> getObjects() {
        return objectsProxyList;
    }

    public List<ReferenceObject> getRefs() {
        return refsOneByObject;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("DeserializationContext:");
        result.append("\n\tobjectsProxyList: ");
        for (ProxyObject object : objectsProxyList){
            result.append("\n\t\t").append(object.toStringFull()).append("");
        }
        result.append("\n\ttempRefs: ");
        for (ReferenceObject ref : tempRefs){
            result.append("\n\t\t").append(ref);
        }
        result.append("\n\trefsOneByObject: ");
        for (ReferenceObject ref : refsOneByObject){
            result.append("\n\t\t").append(ref);
        }
        return result.toString();
    }


    private int numberOfObjects;

    private RiceRegistry riceRegistry;
    private BitIterator refsBlockIterator;
    private BitIterator objectsBlockIterator;

    private List<ProxyObject> objectsProxyList = new ArrayList<>();
    private List<ReferenceObject> tempRefs = new LinkedList<>();
    private List<ReferenceObject> refsOneByObject = new ArrayList<>();
}
