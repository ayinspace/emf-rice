package space.ayin.emf_rice.serialization.entities;

import org.eclipse.emf.ecore.EClass;

public abstract class Reference {
    private EClass refType;//could be supertype of real object type
    private int relativeTypeId = -1;

    public Reference(EClass refType) {
        this.refType = refType;
    }

    public void setRelativeTypeId(int relativeTypeId) {
        this.relativeTypeId = relativeTypeId;
    }

    public int getRelativeTypeId() {
        if (relativeTypeId == -1)
            throw new RuntimeException("Reference is initialized without relativeTypeId. cant get it");
        return relativeTypeId;
    }

    public EClass getRefType() {
        if(refType == null)
            throw new RuntimeException("Reference is initialized without EClass. cant get it");
        return refType;
    }

}
