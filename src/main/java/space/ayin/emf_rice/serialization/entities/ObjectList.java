package space.ayin.emf_rice.serialization.entities;


import java.util.ArrayList;
import java.util.List;

public class ObjectList{
    private List<ReferenceObject> list;

    public ObjectList() {
        this.list = new ArrayList<>();
    }

    public List<ReferenceObject> getList() {
        return list;
    }

    @Deprecated
    public void setList(List<ReferenceObject> list) {
        this.list = list;
    }

    public void add(ReferenceObject ref){
        list.add(ref);
    }

    public int size(){
        return list.size();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("ObjectList{");
        for(ReferenceObject object : list){
            stringBuilder.append(object);
        }
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
