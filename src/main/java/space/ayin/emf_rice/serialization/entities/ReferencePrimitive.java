package space.ayin.emf_rice.serialization.entities;

public class ReferencePrimitive extends Reference {
    private PoolPrimitive primitive;
    public static final int relativeTypeCapacity = 4;

    public ReferencePrimitive(int relativeTypeNumber, PoolPrimitive primitive) {
        super(primitive.getType().geteDataType().eClass());//todo:unchecked
        setRelativeTypeId(relativeTypeNumber);
        this.primitive = primitive;
    }

    public PoolPrimitive getPrimitive() {
        return primitive;
    }

    public static int getRelativeTypeCapacity() {
        return relativeTypeCapacity;
    }

    @Override
    public String toString() {
        return "&" + primitive;
    }
}
