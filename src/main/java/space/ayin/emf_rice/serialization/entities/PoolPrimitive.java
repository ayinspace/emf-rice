package space.ayin.emf_rice.serialization.entities;

import space.ayin.emf_rice.serialization.PrimitiveType;

public class PoolPrimitive {

    private PrimitiveType type;
    private Object object;

    PoolPrimitive(PrimitiveType type){
        this.type = type;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public PrimitiveType getType() {
        return type;
    }

    public void setType(PrimitiveType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append('[');
        if (object == null)
            buffer.append("null");
        else
            buffer.append(object);
        buffer.append(']');
        buffer.append(type.name());
        return buffer.toString();
    }
}
