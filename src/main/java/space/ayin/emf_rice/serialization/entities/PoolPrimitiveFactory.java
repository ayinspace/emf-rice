package space.ayin.emf_rice.serialization.entities;

import com.ubs_soft.commons.collections.BitBuffer;
import com.ubs_soft.commons.collections.BitIterator;
import space.ayin.emf_rice.serialization.PrimitiveType;

import java.nio.ByteBuffer;

public class PoolPrimitiveFactory {

    public static PoolPrimitive createPrimitive(Object object, PrimitiveType primitiveType){
        PoolPrimitive result = new PoolPrimitive(primitiveType);
        result.setObject(object);
        return result;
    }

    public static PoolPrimitive createPrimitive(BitIterator bitIterator, PrimitiveType primitiveType){
        PoolPrimitive result = new PoolPrimitive(primitiveType);
        result.setObject(deserialize(bitIterator, primitiveType));
        return result;
    }

    public static BitBuffer serialize(PoolPrimitive poolPrimitive, BitBuffer bitBuffer){
        PrimitiveType type = poolPrimitive.getType();
        Object factObject = poolPrimitive.getObject();
        if(type == PrimitiveType.String)
            return serialize((String) poolPrimitive.getObject(), bitBuffer);
        else if (type == PrimitiveType.Boolean) {
            return bitBuffer.append((Boolean)factObject);
        }
        else if (type == PrimitiveType.Integer) {
            return bitBuffer.append(intAsByte((Integer)factObject));
        }
        else if (type == PrimitiveType.Float) {
            return bitBuffer.append(floatAsByte((Float)factObject));
        }
        else if (type == PrimitiveType.Double) {
            return bitBuffer.append(doubleAsByte((Double)factObject));
        }
        else if (type == PrimitiveType.Long) {
            return bitBuffer.append(longAsByte((Long)factObject));
        }
        else if (type == PrimitiveType.Short) {
            return bitBuffer.append(shortAsByte((Short)factObject));
        }
        else if (type == PrimitiveType.Byte) {
            return bitBuffer.append((Byte)factObject);
        }
        else if (type == PrimitiveType.Char) {
            return bitBuffer.append(charAsByte((Character) factObject));
        }
        else {
            throw new RuntimeException("not supported primitive class");
        }
    }

    public static Object deserialize(BitIterator bitIterator, PrimitiveType primitiveType){
        if(primitiveType == PrimitiveType.String){
            boolean isNotEmpty = bitIterator.nextBoolean();
            if(!isNotEmpty)
                return "";
            StringBuilder sb = new StringBuilder();
            char ch = byteAsChar(bitIterator.nextBits(16));
            while (Character.MIN_VALUE != ch){
                sb.append(ch);
                ch = byteAsChar(bitIterator.nextBits(16));
            }
            return sb.toString();
        } else if (primitiveType == PrimitiveType.Boolean) {
            return bitIterator.nextBoolean();
        }
        else if (primitiveType == PrimitiveType.Integer) {
            return byteAsInt(bitIterator.nextBits(primitiveType.getLength()));
        }
        else if (primitiveType == PrimitiveType.Float) {
            return byteAsFloat(bitIterator.nextBits(primitiveType.getLength()));
        }
        else if (primitiveType == PrimitiveType.Double) {
            return byteAsDouble(bitIterator.nextBits(primitiveType.getLength()));
        }
        else if (primitiveType == PrimitiveType.Long) {
            return byteAsLong(bitIterator.nextBits(primitiveType.getLength()));
        }
        else if (primitiveType == PrimitiveType.Short) {
            return byteAsShort(bitIterator.nextBits(primitiveType.getLength()));
        }
        else if (primitiveType == PrimitiveType.Byte) {
            return bitIterator.nextBits(primitiveType.getLength());
        }
        else if (primitiveType == PrimitiveType.Char) {
            return byteAsChar(bitIterator.nextBits(primitiveType.getLength()));
        }
        else {
            throw new RuntimeException("not supported primitive class");
        }
    }

    private static BitBuffer serialize(String str, BitBuffer bitBuffer){
        if(str.length() == 0)
            return bitBuffer.append(false);
        else
            bitBuffer.append(true);
        char[] chars = str.toCharArray();
        for(char ch : chars){
            bitBuffer.append(charAsByte(ch), 16) ;
        }
        bitBuffer.append(charAsByte(Character.MIN_VALUE), 16);
        return bitBuffer;
    }

    private static byte[] longAsByte(long var) {
        ByteBuffer b = ByteBuffer.allocate(8);
        b.putLong(var);
        b.rewind();
        byte[] result = new byte[8];
        b.get(result);
        return result;
    }

    private static byte[] intAsByte(int var) {
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putInt(var);
        b.rewind();
        byte[] result = new byte[4];
        b.get(result);
        return result;
    }

    private static byte[] shortAsByte(short var) {
        ByteBuffer b = ByteBuffer.allocate(2);
        b.putShort(var);
        b.rewind();
        byte[] result = new byte[2];
        b.get(result);
        return result;
    }

    private static byte[] charAsByte(char var) {
        ByteBuffer b = ByteBuffer.allocate(2);
        b.putChar(var);
        b.rewind();
        byte[] result = new byte[2];
        b.get(result);
        return result;
    }

    private static byte[] floatAsByte(float var) {
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putFloat(var);
        return b.array();
    }

    private static byte[] doubleAsByte(double var) {
        ByteBuffer b = ByteBuffer.allocate(8);
        b.putDouble(var);
        return b.array();
    }

    private static long byteAsLong(byte[] bytes) {
        int length = Long.BYTES;
        if (bytes.length != length)
            throw new IllegalArgumentException("bytes length is too big");
        ByteBuffer b = ByteBuffer.allocate(length);
        b.position(length - bytes.length);
        b.put(bytes);
        b.rewind();
        return b.getLong();
    }

    private static double byteAsDouble(byte[] bytes) {
        int length = Double.BYTES;
        if (bytes.length != length)
            throw new IllegalArgumentException("bytes length is too big");
        ByteBuffer b = ByteBuffer.allocate(length);
        b.position(length - bytes.length);
        b.put(bytes);
        b.rewind();
        return b.getDouble();
    }

    private static int byteAsInt(byte[] bytes) {
        int length = Integer.BYTES;
        if (bytes.length != length)
            throw new IllegalArgumentException("bytes length is too big");
        ByteBuffer b = ByteBuffer.allocate(length);
        b.position(length - bytes.length);
        b.put(bytes);
        b.rewind();
        return b.getInt();
    }

    private static float byteAsFloat(byte[] bytes) {
        int length = Float.BYTES;
        if (bytes.length > length)
            throw new IllegalArgumentException("bytes length is too big");
        ByteBuffer b = ByteBuffer.allocate(length);
        b.position(length - bytes.length);
        b.put(bytes);
        b.rewind();
        return b.getFloat();
    }

    private static short byteAsShort(byte[] bytes) {
        int length = Short.BYTES;
        if (bytes.length != length)
            throw new IllegalArgumentException("bytes length is too big");
        ByteBuffer b = ByteBuffer.allocate(length);
        b.position(length - bytes.length);
        b.put(bytes);
        b.rewind();
        return b.getShort();
    }

    private static char byteAsChar(byte[] bytes) {
        int length = Character.BYTES;
        if (bytes.length != length)
            throw new IllegalArgumentException("bytes length is wrong");
        ByteBuffer b = ByteBuffer.allocate(length);
        b.put(bytes);
        b.rewind();
        return b.getChar();
    }
}
