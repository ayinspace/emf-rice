package space.ayin.emf_rice.serialization.entities;

import org.eclipse.emf.ecore.EClass;

public class ReferenceObject extends Reference {

    private ProxyObject proxyObject;
    private boolean isNull = false;
    private boolean isTemp = false;
    private int tempAddr = -1;

    public ReferenceObject(EClass refType, int relativeTypeId, ProxyObject proxyObject) {
        super(refType);
        setRelativeTypeId(relativeTypeId);
        this.proxyObject = proxyObject;
    }

    public static ReferenceObject getNullReferenceObject(EClass refType) {
        ReferenceObject result = new ReferenceObject(refType);
        result.isNull = true;
        return result;
    }

    public static ReferenceObject getTempReferenceObject(EClass refType, int tempAddr) {
        ReferenceObject result = new ReferenceObject(refType);
        result.isTemp = true;
        result.tempAddr = tempAddr;
        return result;
    }

    private ReferenceObject(EClass refType){
        super(refType);
    }

    private ReferenceObject(EClass refType, int relativeTypeId) {
        super(refType);
        setRelativeTypeId(relativeTypeId);
    }

    public ProxyObject getProxyObject() {
        return proxyObject;
    }

    public boolean isNull() {
        return isNull;
    }


    public boolean isTemp() {
        return isTemp;
    }

    public int getTempAddr() {
        return tempAddr;
    }

    public void setProxyObject(ProxyObject proxyObject) {
        this.proxyObject = proxyObject;
    }

    public void makeNotTemp(ProxyObject proxyObject){
        this.proxyObject = proxyObject;
        isTemp = false;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("&");
        if(isNull)
            result.append("null");
        else{
            if(isTemp){
                result.append("(tmpAddr:").append(tempAddr).append(")");
            }
            result.append(proxyObject);
        }
        return result.toString();
    }
}
