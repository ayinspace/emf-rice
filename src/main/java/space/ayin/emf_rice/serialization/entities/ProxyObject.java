package space.ayin.emf_rice.serialization.entities;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import java.util.ArrayList;
import java.util.List;

public class ProxyObject{
    private int poolAddress;
    private EClass objectType;
    private ArrayList<ReferenceObject> objectRefs = new ArrayList<ReferenceObject>();
    private ArrayList<ReferencePrimitive> primitiveRefs = new ArrayList<ReferencePrimitive>();
    private EObject emfObject = null;
    private List<ObjectList> objectLists = new ArrayList<>();


    public ProxyObject(int poolAddress) {
        this.poolAddress = poolAddress;
    }

    public void setObjectType(EClass objectType) {
        this.objectType = objectType;
    }

    public void addObjectRef(ReferenceObject reference){
        objectRefs.add(reference);
    }

    public void addPrimitiveRef(ReferencePrimitive reference){
        primitiveRefs.add(reference);
    }

    public int getPoolAddress() {
        return poolAddress;
    }

    public EClass getObjectType() {
        return objectType;
    }

    public ArrayList<ReferenceObject> getObjectRefs() {
        return objectRefs;
    }

    public ArrayList<ReferencePrimitive> getPrimitiveRefs() {
        return primitiveRefs;
    }

    public EObject getEmfObject() {
        return emfObject;
    }

    public void setEmfObject(EObject emfObject) {
        this.emfObject = emfObject;
    }

    public List<ObjectList> getObjectLists() {
        return objectLists;
    }

    public void addObjectsList(ObjectList objectList){
        objectLists.add(objectList);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Object[");
        stringBuilder.append(poolAddress).append("]: ");

        if(objectType!=null)
            stringBuilder.append("{type=").append(objectType).append("}, ");

        if(emfObject!=null)
            stringBuilder.append("{emfObject=").append(emfObject).append("}, ");

        return stringBuilder.toString();
    }


    public String toStringFull() {
        StringBuilder stringBuilder = new StringBuilder("Object[");
        stringBuilder.append(poolAddress).append("]: ");

        if(objectType!=null)
            stringBuilder.append("{type=").append(objectType).append("}, ");

        if(emfObject!=null)
            stringBuilder.append("{emfObject=").append(emfObject).append("}, ");

        stringBuilder.append("primitiveRefs{");
        for(ReferencePrimitive primitiveRef : primitiveRefs){
            stringBuilder.append(primitiveRef).append(" ");
        }
        stringBuilder.replace(stringBuilder.length()-1,stringBuilder.length(),"}, ");

        stringBuilder.append("objectRefs{");
        for(ReferenceObject objectRef : objectRefs){
            stringBuilder.append(objectRef).append(" ");
        }
        stringBuilder.replace(stringBuilder.length()-1,stringBuilder.length(),"}, ");

        stringBuilder.append("objectLists{");
        for(ObjectList objectList : objectLists){
            stringBuilder.append(objectList).append(" ");
        }
        stringBuilder.replace(stringBuilder.length()-1,stringBuilder.length(),"}, ");

        return stringBuilder.toString();
    }
}
