package space.ayin.emf_rice.serialization;

import javafx.util.Pair;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import space.ayin.emf_rice.RiceRegistry;
import space.ayin.emf_rice.RiceRegistryInst;
import space.ayin.emf_rice.serialization.entities.*;

import java.util.*;

/** sorted collection of all objects needed for serialization*/
public class SerializationContext {
    private RiceRegistry registry;

    private LinkedList<ProxyObject> objectsProxyList = new LinkedList<>();
    private LinkedList<ReferenceObject> objectsReferenceList = new LinkedList<>();

    private Map<EObject, ProxyObject> objectsProxyMap = new HashMap<>();
    private int nextObjectNumber = -1;

    public SerializationContext(EClass eClass, EObject factObject){
        registry = new RiceRegistryInst(eClass);
        createProxyRef(eClass, factObject);
        objectsProxyList.sort(Comparator.comparingInt(ProxyObject::getPoolAddress));
        objectsReferenceList.sort(Comparator.comparingInt(value -> value.getProxyObject().getPoolAddress()));
    }

    /**
     *
     * @param eClass
     * @param factObject can be null : special ReferenceObject for null created
     * @return
     */
    private ReferenceObject createProxyRef(EClass eClass, EObject factObject){
        if(factObject == null){
            return ReferenceObject.getNullReferenceObject(eClass);
        }
        int relativeTypeId = registry.resolveToNumber(eClass , EMFFactory.classOf(factObject));
        ReferenceObject ref;
        if(EMFFactory.isPrimitive(eClass)){
            //todo: remove
            throw new RuntimeException("createProxyRef called with primitive EClass");
        }
        else
            ref = addObject(eClass, relativeTypeId, factObject);
        return ref;
    }

    private ProxyObject createProxyObject(EObject factObject){
        int number = nextObjectNumber();
        ProxyObject proxy = new ProxyObject(number);

        for(Pair<EDataType, Object> pairOfPrimitive : EMFFactory.containedAttributes(factObject)){
            ReferencePrimitive ref = createPrimitiveReference(pairOfPrimitive.getKey(), pairOfPrimitive.getValue());
            proxy.addPrimitiveRef(ref);
        }

        for(Pair<EClass,EObject> pairOfObject : EMFFactory.containedReferences(factObject)){
            ReferenceObject ref = createProxyRef(pairOfObject.getKey(), pairOfObject.getValue());
            proxy.addObjectRef(ref);
        }

        for(Pair<EClass, List<EObject>> pairOfObject : EMFFactory.containedLists(factObject)){
            ObjectList objectList = new ObjectList();
            for(EObject eObject : pairOfObject.getValue()){
                ReferenceObject ref = createProxyRef(pairOfObject.getKey(), eObject);
                objectList.add(ref);
            }
            proxy.addObjectsList(objectList);
        }

        return proxy;
    }

    private ReferencePrimitive createPrimitiveReference(EDataType dataType, Object factObject){
        PoolPrimitive proxy = createPrimitive(dataType, factObject);
        int relativeTypeId = 0;//todo
        return new ReferencePrimitive(relativeTypeId, proxy);
    }

    private PoolPrimitive createPrimitive(EDataType datType, Object factObject){
        return PoolPrimitiveFactory.createPrimitive(factObject, PrimitiveType.typeFor(datType));//todo: type check or code rework
    }

    private ReferenceObject addObject(EClass eClass, int relativeTypeId, EObject eObject) {
        ReferenceObject referenceObject;
        ProxyObject proxyObject;
//        objectsRefsList.add((ReferenceObject) ref);
        if (objectsProxyMap.containsKey(eObject)) {
            proxyObject = objectsProxyMap.get(eObject);
            referenceObject = new ReferenceObject(eClass, relativeTypeId, proxyObject);
        } else {
            proxyObject = createProxyObject(eObject);
            objectsProxyMap.put(eObject,proxyObject);
            objectsProxyList.add(proxyObject);
            referenceObject = new ReferenceObject(eClass, relativeTypeId, proxyObject);
            objectsReferenceList.add(referenceObject);
        }
        return referenceObject;
    }

    private int nextObjectNumber() {
        nextObjectNumber+=1;
        return nextObjectNumber;
//        return objectsProxyList.size();
    }

    List<ProxyObject> getObjects(){
        return objectsProxyList;
    }

    List<ReferenceObject> getRefs(){
        return objectsReferenceList;
    }

    public int size(){
        return objectsProxyList.size();
    }

    public RiceRegistry getRegistry() {
        return registry;
    }


    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("SerializationContext:");
        result.append("\n\tobjectsProxyList: ");
        for (ProxyObject object : objectsProxyList){
            result.append("\n\t\t").append(object.toStringFull()).append("");
        }
        result.append("\n\tobjectsReferenceList: ");
        for (ReferenceObject ref : objectsReferenceList){
            result.append("\n\t\t").append(ref);
        }
        result.append("\n\tobjects-EObjectsMap: ");
        for (EObject eObject : objectsProxyMap.keySet()){
            result.append("\n\t\t").append(eObject.eClass().getName()).append("@").append(Integer.toHexString(eObject.hashCode())).append(": ").append(objectsProxyMap.get(eObject));
        }
        return result.toString();
    }
}
