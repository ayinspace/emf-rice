package space.ayin.emf_rice.serialization;

import com.ubs_soft.commons.collections.BitBuffer;
import com.ubs_soft.commons.collections.BitIterator;
import com.ubs_soft.commons.collections.GrowingArrayBitBuffer;
import org.eclipse.emf.ecore.EClass;
import space.ayin.emf_rice.RiceRegistry;
import space.ayin.emf_rice.serialization.entities.*;

public class Serializer {

    public static final int BITS_FOR_POOL_SIZE = 8;
    public static final int BITS_FOR_REFS_BLOCK_SIZE = 8;

    private static RiceRegistry riceRegistry;

    public static void setRiceRegistry(RiceRegistry riceRegistry) {
        Serializer.riceRegistry = riceRegistry;
    }

    public static BitBuffer serialize(SerializationContext pool) {
        riceRegistry = pool.getRegistry();

        //createRefsBlock
        BitBuffer refsBuffer = new GrowingArrayBitBuffer();
        for (ReferenceObject reference : pool.getRefs()) {
            int capacity = riceRegistry.capacity(reference.getRefType());
            //if capacity = 1 then we have no type variations so we can skip this refs in serialization and deserialization
            if (capacity > 1) {
                refsBuffer.appendMaxNumber(reference.getRelativeTypeId(), capacity);
            }
        }

        //createObjectsBlock
        BitBuffer objectsBuffer = new GrowingArrayBitBuffer();
        int bitsPerObjectAddress = BitBuffer.quantityOfBitsForNumber(pool.size());
        for (ProxyObject object : pool.getObjects()) {
            appendObjectData(object, objectsBuffer, bitsPerObjectAddress);
        }

        //creation starting bits for message
        BitBuffer headerBuffer = new GrowingArrayBitBuffer();
        headerBuffer.append(pool.size(), BITS_FOR_POOL_SIZE);
        headerBuffer.append(refsBuffer.getBitLength(), BITS_FOR_REFS_BLOCK_SIZE);

        BitBuffer resultBuffer = new GrowingArrayBitBuffer();
        resultBuffer.append(headerBuffer);
        resultBuffer.append(refsBuffer);
        resultBuffer.append(objectsBuffer);
        return resultBuffer;
    }

    private static void appendObjectData(ProxyObject object, BitBuffer objectsBuffer, int bitsPerObjectAddress) {
        for (ReferencePrimitive primitiveRef : object.getPrimitiveRefs()) {
            appendPrimitiveRef(primitiveRef, objectsBuffer);
        }
        for (ReferenceObject objectRef : object.getObjectRefs()) {
            appendObjectRef(objectRef, objectsBuffer, bitsPerObjectAddress);
        }
    }

    private static void appendPrimitiveRef(ReferencePrimitive ref, BitBuffer objectsBuffer) {
        PoolPrimitive primitive = ref.getPrimitive();
        //todo:primitive ref
        PoolPrimitiveFactory.serialize(primitive, objectsBuffer);
    }

    private static void appendObjectRef(ReferenceObject ref, BitBuffer objectsBuffer, int bitsPerObjectAddress) {
        if (ref.isNull()) {
            objectsBuffer.append(false);
        } else {
            objectsBuffer.append(true);
            ProxyObject member = ref.getProxyObject();
            objectsBuffer.append(member.getPoolAddress(), bitsPerObjectAddress);
        }
    }

    public static DeserializationContext deserialize(BitBuffer messageBitBuffer, EClass eClass) {
        BitIterator mainIterator = messageBitBuffer.bitIterator();

        Integer exactNumberOfObjects = mainIterator.nextNumber(BITS_FOR_POOL_SIZE);
        Integer refsBlockLength = mainIterator.nextNumber(BITS_FOR_REFS_BLOCK_SIZE);

        BitIterator refsBlockIterator = mainIterator.doubler();

        mainIterator.skip(refsBlockLength);

        DeserializationContext deserializationContext = new DeserializationContext(refsBlockIterator, mainIterator, eClass, exactNumberOfObjects);

        /** Read {@link BITS_FOR_POOL_SIZE} bits, convert them in number. this number is exact number of objects in message
         * DEserialization pool(tree) filling with objects data(in form of BitBuffers or byte arrays) + relative objects addreses
         recursively creating actual objects from byte data and addreses converted to objects */

        /**
         * Read first object typeId
         * find actual objectType
         * look at objectTypePossibleContainments
         * createProxyObject add all primitive data and link addresses
         * repeat creating proxyObjects
         * */
        return deserializationContext;
    }
}
