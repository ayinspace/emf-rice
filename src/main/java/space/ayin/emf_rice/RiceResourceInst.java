package space.ayin.emf_rice;

import com.ubs_soft.commons.collections.BitBuffer;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import space.ayin.emf_rice.serialization.DeserializationContext;
import space.ayin.emf_rice.serialization.EMFFactory;
import space.ayin.emf_rice.serialization.SerializationContext;
import space.ayin.emf_rice.serialization.Serializer;

public class RiceResourceInst implements RiceResource {

    //private Serializer serializer = new Serializer();
    private EMFFactory emfFactory = new EMFFactory();
    private EClass fieldType;
    private byte[] serialized;
    private EObject factObject;
    private BitBuffer buffer;

    public RiceResourceInst(EClass fieldType, EObject factObject){
        this.fieldType = fieldType;
        SerializationContext pool = new SerializationContext(fieldType, factObject);
        System.out.println(pool);
        buffer = Serializer.serialize(pool);
        //serialized = buffer.getData();
    }


    public RiceResourceInst(EClass fieldType, BitBuffer buffer){
        this.fieldType = fieldType;
        this.buffer = buffer;
//        serialized = buffer.getData();
        DeserializationContext deserializationContext = Serializer.deserialize(buffer, fieldType);
        factObject = emfFactory.init(fieldType, deserializationContext);
    }

    @Override
    public BitBuffer serialize() {
        return buffer;
    }

    @Override
    public EObject deserialize() {
        return factObject;
    }


}
