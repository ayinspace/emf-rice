package space.ayin.emf_rice;


import com.ubs_soft.commons.collections.BitBuffer;

public interface RiceResource {
    BitBuffer serialize();
    Object deserialize();
}
