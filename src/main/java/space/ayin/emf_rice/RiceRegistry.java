package space.ayin.emf_rice;

import org.eclipse.emf.ecore.EClass;

public interface RiceRegistry {

    EClass resolveToClass(EClass fieldType, int number);
    int  resolveToNumber(EClass fieldType, EClass objectType);
    int capacity(EClass fieldType);

}
