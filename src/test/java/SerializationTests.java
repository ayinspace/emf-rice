import cardgame_model.cardgame_model.*;
import cardgame_model.cardgame_model.impl.Cardgame_modelPackageImpl;
import com.ubs_soft.commons.collections.BitBuffer;
import com.ubs_soft.commons.collections.BitBufferFormatter;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.junit.Test;
import space.ayin.emf_rice.RiceRegistryInst;
import space.ayin.emf_rice.RiceResourceInst;
import space.ayin.emf_rice.serialization.EMFFactory;

import static org.junit.Assert.assertTrue;

public class SerializationTests {
    @Test
    public void stringlessCardTest() {
        ((Cardgame_modelPackageImpl) Cardgame_modelPackage.eINSTANCE).createPackageContents();
        EClass abstractGameCardClass = Cardgame_modelPackage.eINSTANCE.getAbstractGameCard();
        RiceRegistryInst registry = new RiceRegistryInst(abstractGameCardClass);

        Game game = Cardgame_modelFactory.eINSTANCE.createGame();
        CardGameSet set = Cardgame_modelFactory.eINSTANCE.createCardGameSet();
        set.setGame(game);
        StringlessCard stringlessCard = Cardgame_modelFactory.eINSTANCE.createStringlessCard();
        stringlessCard.setCardgameset(set);

        System.out.println(stringlessCard);

        RiceResourceInst initialResource = new RiceResourceInst(abstractGameCardClass, stringlessCard);
        BitBuffer bitBuffer = initialResource.serialize();

        BitBufferFormatter bitBufferFormatter = bitBuffer.bitBufferFormatter()
                .setByteView(BitBufferFormatter.ByteView.BitsBigEndian)
                .setByteOrder(BitBufferFormatter.ByteOrder.BigEndian)
                .setShort(false);

        System.out.println("serialized bytes:\n" + bitBuffer);

        RiceResourceInst recieverResource = new RiceResourceInst(abstractGameCardClass, bitBuffer);

        EObject deserialised = recieverResource.deserialize();
        System.out.println(deserialised);

        assertTrue(EMFFactory.equals(stringlessCard, deserialised));
    }

    @Test
    public void mtgCardTest() {
        ((Cardgame_modelPackageImpl) Cardgame_modelPackage.eINSTANCE).createPackageContents();
        EClass abstractGameCardClass = Cardgame_modelPackage.eINSTANCE.getAbstractGameCard();
        RiceRegistryInst registry = new RiceRegistryInst(abstractGameCardClass);

        MtgCard mtgCard = Cardgame_modelFactory.eINSTANCE.createMtgCard();
        mtgCard.setText("w");
        System.out.println(mtgCard);

        RiceResourceInst initialResource = new RiceResourceInst(abstractGameCardClass, mtgCard);
        BitBuffer bitBuffer = initialResource.serialize();

        BitBufferFormatter bitBufferFormatter = bitBuffer.bitBufferFormatter()
                .setByteView(BitBufferFormatter.ByteView.BitsBigEndian)
                .setByteOrder(BitBufferFormatter.ByteOrder.BigEndian)
                .setShort(false);

        System.out.println("serialized bytes:\n" + bitBuffer);

        RiceResourceInst recieverResource = new RiceResourceInst(abstractGameCardClass, bitBuffer);

        EObject deserialised = recieverResource.deserialize();
        System.out.println(deserialised);

        assertTrue(EMFFactory.equals(mtgCard, deserialised));
    }
}