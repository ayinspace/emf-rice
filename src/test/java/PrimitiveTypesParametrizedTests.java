import com.ubs_soft.commons.collections.BitBuffer;
import com.ubs_soft.commons.collections.GrowingArrayBitBuffer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import space.ayin.emf_rice.serialization.PrimitiveType;
import space.ayin.emf_rice.serialization.entities.PoolPrimitive;
import space.ayin.emf_rice.serialization.entities.PoolPrimitiveFactory;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class PrimitiveTypesParametrizedTests {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        //int[] ints = new int[]{1234};
        return Arrays.asList(new Object[][] {
                { -1, Integer.TYPE}, { 3337, Integer.TYPE},{ "bla", String.class}, { "wrath of gods", String.class}, { "3287598324759215", String.class}
        });
    }
    @Parameterized.Parameter
    public Object object;
    @Parameterized.Parameter(1)
    public Class aClass;

    @Test
    public void objectSerializationTest(){
        PrimitiveType type = PrimitiveType.typeFor(aClass);
        PoolPrimitive primitive = PoolPrimitiveFactory.createPrimitive(object, type);
        BitBuffer bitBuffer = new GrowingArrayBitBuffer();
        PoolPrimitiveFactory.serialize(primitive, bitBuffer);
        Object deserialized = PoolPrimitiveFactory.deserialize(bitBuffer.bitIterator(), type);
        assertEquals(object, deserialized);
    }
}
